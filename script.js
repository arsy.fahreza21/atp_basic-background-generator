var css = document.querySelector("h3");
var color1 = document.querySelector(".color1");
var color2 = document.querySelector(".color2");
var body = document.getElementById("gradient");
var directionSelect = document.getElementById("direction");
var gradientTypeSelect = document.getElementById("gradient-type");

function setGradient() {
  var gradientType = gradientTypeSelect.value;
  var direction = directionSelect.value;
  
  if (gradientType === 'linear') {
    body.style.background = "linear-gradient(" + direction + ", " + color1.value + ", " + color2.value + ")";
  } else if (gradientType === 'radial') {
    body.style.background = "radial-gradient(" + color1.value + ", " + color2.value + ")";
  }

  css.textContent = body.style.background + ";";
}

color1.addEventListener("input", setGradient);
color2.addEventListener("input", setGradient);
directionSelect.addEventListener("change", setGradient);
gradientTypeSelect.addEventListener("change", setGradient);
